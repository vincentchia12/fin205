---
title: "__FIN205__
        <br>
        Data Technologies for Financial Modelling
        <br>
        <span style='font-size:75%;'>Study Unit 1</span>
        <br>"
author: "<span style='color:green;font-size:60%;'>last updated on  2021 - 1 - 16</span>"
output: 
  revealjs::revealjs_presentation:
    css: styles.css
    self_contained: false
    reveal_plugins: ["notes", "chalkboard", "menu"]
    transition: fade
    reveal_options:
      slideNumber: true
      chalkboard:
        theme: whiteboard
        toggleNotesButton: false
      menu:
        side: right
    theme: sky
    highlight: pygments
params:
  quizzes_pct: 6%
  clspart_pct: 6%
  tma_pct: 18%
  gba_pct: 20%
  tma_deadline: !r format(ISOdate(2021,2,15,23,55,0,tz=""), "%A, %d %B %Y, %H:%M:%S")
  gba_deadline: !r format(ISOdate(2021,2,26,23,55,0,tz=""), "%A, %d %B %Y, %H:%M:%S")
  exam_pct: 50%
  pcoq_deadline: !r base::format(base::ISOdate(2021,1,22,12,00,0,tz=""), "%A, %d %B %Y, %H:%M:%S")
  gba_grouping_deadline: !r base::format(base::ISOdate(2021,2,5,0,0,0,tz=""), "%d %B %Y")
---


