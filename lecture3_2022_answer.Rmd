---
title: "__FIN205__
        <br>
        Data  Technologies  for  Financial  Modelling
        <br>
        <span style='font-size:75%;'>Lesson 3: MCQ Answers</span>
        <br><br>"
author: "<span style='color:green;font-size:60%;'>last updated on 2nd Feb 2022</span>"
output: 
  revealjs::revealjs_presentation:
    css: styles.css
    self_contained: true
    transition: fade
    reveal_options:
      slideNumber: true
      chalkboard:
        theme: whiteboard
        toggleNotesButton: false
      menu:
        side: right
    theme: sky
    highlight: pygments
---

# Answer for MCQ 1 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*Why Indirect method is often used by External Financial Analysts to compute Cashflow for Operating ?*
<br>

*a. Balance Sheet and Income Statement are usually and easily available*
</section>


# Answer for MCQ 2 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*Direct and Indirect Method of computations are meant for*
<br>

*c. Cashflow for Operating, CFO*
</section>


# Answer for MCQ 3 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*What is NOT an example of Current Asset ?*
<br>

*d. Accounts payable*
</section>


# Answer for MCQ 4 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*What is NOT an example of Fixed Asset*
<br>

*a. Cash and Cash Equivalents*
</section>


# Answer for MCQ 5 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*Inventory falls under*
<br>

*b. Current Assets*
</section>


# Answer for MCQ 6 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*Goodwill is considered as*
<br>

*a. Intangible Asset*
</section>


# Answer for MCQ 7 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*Which has the potential to be written-off ?*
<br>

*c. Bad debts* 
</section>


# Answer for MCQ 8 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*For CFO[Indirect Method] calculation,*
<br>
*which Noncash charge(s) is/are added back to Net Income :*
<br><br>
*(i). Depreciation Expense*
<br>
*(ii). Amortization Expense*
<br>

*c. (i) & (ii)*
</section>


# Answer for MCQ 9 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br><br>
*One easy way to compute Depreciation Expense is*
<br>

*c. Straight-line depreciation method*
</section>


# Answer for MCQ 10 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Cost of Goods Sold can be computed from:*
<br>

*d.  Opening Inventories + Purchases - Closing Inventories*
</section>