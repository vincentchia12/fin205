---
title: "__FIN205__
        <br>
        Data Technologies for Financial Modelling
        <br>
        <span style='font-size:75%;'>Lesson 5</span>
        <br>"
author: "<span style='color:green;font-size:60%;'>last updated on 23rd Feb 2023</span>"
output: 
  revealjs::revealjs_presentation:
    css: styles.css
    self_contained: true
#    reveal_plugins: ["notes", "chalkboard", "menu"]
#    transition: fade
#    reveal_options:
#      slideNumber: true
#      chalkboard:
#        theme: whiteboard
#      toggleNotesButton: false
#      menu:
#        side: right
    theme: sky
    highlight: pygments
---

# Financial Derivatives Valuation {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*In this unit, we will explore and have a better understanding on*
<br><br>
*Equity Index Derivatives, says, Equity Index Futures*
<br><br>
*Equity Derivatives valuation, says, Equity Vanilla Options*
</section>


# Assets {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-1.png")
```
</section>


# Financial Derivatives {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
<br>
*A __financial derivative__ is a financial contract that derives*
*its value from an underlying asset.*
<br><br><br>
*Common underlying instruments include bonds, commodities,*
*currencies, interest rates, financial market indexes, and common stocks.* 
<br><br><br>
*__Financial derivatives__ can be used to either mitigate risk (hedging) or* 
*assume risk with the expectation of commensurate reward (speculation).*
</section>


# Financial Derivatives' Markets {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
<br><br>
*__Exchange-traded__*
<br>
*Standardised, backed by clearinghouse* 
<br><br>
*example: Futures, Vanilla Option contracts and etc.*
<br><br><br>
*__Over-The-Counter (OTC)__*
<br>
*A dealer market with no central location, unregulated, deals directly*
<br>
*with counterparty.*
<br><br>
*example: Forwards, Vanilla and Exotic Options, etc.*
</section>


# Financial Derivative {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
<br>
*__Forward commitment:__*
<br>
*Legally binding promise to perform some action in the future.*
<br><br>
*Example:*
<br>
*Futures contracts, Forwards, Swaps.* 
<br><br><br><br>
*__Contingent claim:__*
<br>
*A claim (to a payoff) that depends on a particular event.*
<br><br>
*Example:*
<br>
*Equity Vanilla Options whose values depend on Common Stock prices.*
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Futures & Forwards</h1>


# Futures contract vs Forward {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Futures contract:__*
<br>
*A forward contract that is standardized & exchange-traded.*
<br>
*Regulated, backed by clearinghouse or exchanges*; 
<br><br><br>
*__Forwards:__*
<br>
*One party agrees to buy and the counterparty to sell a* *physical or financial asset at a specific price on a specific date in the future.*
<br>
*Private, customized contract usually traded in Over-The-Counter (OTC) markets.*
</section>


# Major Differences {.align_left .h1_to_h2} 
<br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-5.png")
```


# DJIA's Futures Chain {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-3.png")
```


# Asset's & Futures' prices {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-4.png")
```


# Payoff of a futures contract {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*__Spot Price, S(t)__:*
<br>
*is the market price of the underlying asset.*
<br><br><br>
*__Price Hedging on the underlying asset on a future date__*
<br>
*may be done via a Futures or Forward contract,* 
<br>
*as the financial derivative eliminates or reduces uncertainty about future price of an asset.*
<br><br>
*If the underlying asset S(t) rises over the life of the contract to S(T)*
<br>
*T is the expiry date of the futures contract of concern*
<br>
*then the Payoff is positive. __Payoff = S(T) - F(0)  >  0__*
<br><br><br>
*Note:*
<br>
*Party who buys = opens a long position in the forward or futures, and is called __the long.__*
<br>
*Party who sells = opens a short position in the forward or futures, and is called __the short.__*
</section>


# Futures Price will converge towards the evolved Spot Price {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-6.png")
```


# Asset's & Futures' prices converge {.align_left .h1_to_h2} 
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-7.png")
```


# OPENING a LONG POSITION (TO BUY) {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*He or she hopes to buy low and sell high.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-10.png")
```
</section>


# OPENING a SHORT POSITION (TO SELL) {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*He or she hopes to sell high and buy low.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-11.png")
```
</section>


# Open a Long Position in (Buy) Futures {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-12.png")
```


# Open a Long Position in Futures {.align_left .h1_to_h2} 
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-13.png")
```


# Open a Long Position in Futures {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-14.png")
```


# Open a Long Position in Futures {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-15.png")
```


# Open a Short Position in (Sell) Futures {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-16.png")
```


# Open a Short Position in Futures {.align_left .h1_to_h2} 
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-17.png")
```


# Open a Short Position in Futures {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-18.png")
```


# Open a Short Position in Futures {.align_left .h1_to_h2}  
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-19.png")
```


# Futures Pricing & Valuation {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:70%;">
<br>
*The futures price, that is, the price at which the buyer commits to purchase*
<br>
*the underlying asset can be calculated using the following formulas:*
<br><br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-125.png")
```
</section>


# Futures Pricing & Valuation {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-126.png")
```


# Illustration: Exposure {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:70%;">
<br><br>
*Your company gives its customers an option to pay in bitcoin.* 
<br><br>
*Roughly 0.5% of your monthly revenue is collected in bitcoins* 
*so you haven’t felt the need to hedge the exposure.* 
<br><br>
*However, your sales team tells you that recently they recently signed a contract* 
*that will result in a particularly large payment of 200 bitcoins in 3-months.*
<br><br>
*Due to extreme volatility in the cryptocurrencies,* 
*you decide to hedge the exposure.*
</section>


# Illustration: Exposure Hedged {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:70%;">
<br>
*CBOE offers bitcoin futures with a multiplier of one and* 
*because you current have a long position in bitcoin,* 
*so you must get a short position in bitcoin futures,* 
*that is, you must sell 200 futures.*
<br><br>
*You sell 200 XBT/K8 contracts with a futures value of $6,820.*
<br>
*The contract matures in 3 months or 90 days.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-153.png")
```
</section>


# Exposure Calculation {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:70%;">
*The value of your contract at inception is zero.*
<br>
*At the settlement date,* 
*if the bitcoin price moves to $6,400,* 
*the payoff to the party with long position is as follows:*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-129.png")
```
<br>
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/fd-154.png")
```
</section>


# Hedging Explanation {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*As you have short position, you will payoff will be exactly opposite to the payoff to the long position.*
*Your profit amounts to $84,000.*
<br>
*By selling the futures you have guaranteed that you get at least $6,820 per bitcoin no matter what happens to the bitcoin price.*
<br>
*You can sell the 200 bitcoins at the spot price at expiration of $6,400* 
*for proceeds of $1,280,000 and receive $84,000 as profit on the futures contract.* 
<br>
*Your net cash flows are $1,364,000; which equals the price you locked: 200 x $6,820.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-155.png")
```
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Vanilla Options</h1>


# Vanilla Options {.align_left .h1_to_h2} 
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-20.png")
```


# Speculation with Options {.align_left .h1_to_h2} 
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-23.png")
```


# Futures & Vanilla Options {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-27.png")
```
 

# Futures & Vanilla Options {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-28.png")
```
 

# Long Positions in Vanilla Call vs Futures {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-31.png")
```


# Options Opening Positions (Long=Buy, SHORT=Sell) {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Long call:__ The buyer of a vanilla call option.*
<br>
*Gives the buyer the __right to buy__ an underlying asset.*
<br><br>
*__Short call:__ The seller (writer) of a vanilla call option.*
<br>
*Has the __obligation to sell__ the underlying asset.*
<br><br><br>
*__Long put:__ The buyer of a vanilla put option.*
<br>
*Gives the buyer the __right to sell__ an underlying asset.*
<br><br>
*__Short put:__ The seller (writer) of a vanilla put option.*
<br>
*Has the __obligation to buy__ the underlying asset.*
</section>


# Buying a Call Option: MONEYNESS {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Moneyness:__ (useful for Option buyers)* 
<br><br>
*An option is In The Money (__ITM__) if immediate exercise generates positive payoff.* 
<br>
*An option is Out of The Money (__OTM__) if immediate exercise result in a loss.*
<br>
*An option is At The Money (__ATM__)*
<br>
*if exercise or strike price = current or Spot price of the underlying asset.*
<br><br><br>
*Let the Strike or Exercise price be K.* 
<br>
*Let Spot price of the underlying asset be S.*
<br><br>
*In The Money (Spot price), __ITM Call__ option: S > K*
<br>
*At The Money (Spot price), __ATM Call__ option: S = K*
<br>
*Out of The Money (Spot price), __OTM Call__ option: S < K*
</section>


# BUYING a Call Option: MONEYNESS {.align_left .h1_to_h2}
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-38.png")
```
 

# Buying a Put Option: MONEYNESS {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*Let the Strike or Exercise price be K.* 
<br>
*Let Spot price of the underlying asset be S.*
<br><br><br><br>
*In The Money (Spot price), __ITM Put__ option: K > S*
<br><br>
*At The Money (Spot price), __ATM Put__ option: K = S*
<br><br>
*Out of The Money (Spot price), __OTM Put__ option: K < S*
</section>


# BUYING a Put Option: MONEYNESS {.align_left .h1_to_h2}
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-39.png")
```


# Vanilla Option's Payoff {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Vanilla Call option's payoff or Intrinsic Value:__* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-101.png")
```
<br><br><br>
*__Vanilla Put option's payoff or Intrinsic Value:__* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-102.png")
```
</section>


# Buying a Vanilla Call: P&L calculation {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-108.png")
```
 
 
# Buying a Vanilla Put: P&L calculation {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-109.png")
```
   

# Factors determining Vanilla Option Premium {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-40.png")
```


# Factor: Spot Price of asset {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*A key factor is: __Spot price of underlying asset, S__*
<br><br><br>
*__Vanilla Call option:__*
<br>
*Higher __S__ => greater intrinsic value, higher option value.*
<br><br>
*__Vanilla Put option:__*
<br>
*Higher __S__ => lesser intrinsic value, lower option value.*
<br><br><br>
*__Vanilla Call option's payoff or Intrinsic Value:__* 
<br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-101.png")
```
<br>
*__Vanilla Put option's payoff or Intrinsic Value:__* 
<br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-102.png")
```
</section>


# Call option: Higher spot price, greater intrinsic value, higher option premium {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-41.png")
```


# A factor: Strike or Exercise Price {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
*Another key factor is: __Strike__ or __Exercise__ price, __K__*
<br><br>
*__Vanilla Call option:__* 
<br>
*Higher Strike or Exercise price => lower option premium.*
<br><br>
*__Vanilla Put option:__*
<br>
*Higher Strike or Exercise price => higher option premium.*
<br><br><br>
*__Vanilla Call option's payoff or Intrinsic Value:__* 
<br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-101.png")
```
<br>
*__Vanilla Put option's payoff or Intrinsic Value:__* 
<br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/fd-102.png")
```
</section>


# Buying Vanilla Calls with different Strike Prices {.align_left .h1_to_h2} 
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-38.png")
```


# Buying Vanilla Puts with different Strike Prices {.align_left .h1_to_h2} 
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-39.png")
```


# A factor: Risk-free interest rate {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*Another key factor is:*
<br>
*__Risk-free interest rate__ (cost of borrowing, financing cost)*
<br><br><br>
*__Vanilla Call option:__*
<br>
*Increase risk-free rate => higher option premium.*
<br><br><br>
*__Vanilla Put option:__*
<br>
*Increase risk-free rate => lower option premium.*
</section>


# Call option: Increase risk-free rate, higher option premium {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-44.png")
```


# Put option: Increase risk-free rate, lower option premium {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-110.png")
```


# A factor: Time to Expiration {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Another key factor is:*
<br>
*__Time to Expiration__*
<br><br>
*__Vanilla Call or Put option:__* 
<br>
*Longer time to expiration => higher option value.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-46.png")
```
</section>


# A factor: volatility {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Another key factor is:*
<br>
*__volatility of underlying asset's price__*
<br><br>
*__Vanilla Call or Put option:__* 
<br>
*Higher volatility => higher option premium.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-50.png")
```
</section>


# Summary: Factors determining Vanilla Option Price {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-58.png")
```


# Option Application: Bullish View {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*Buy a vanilla call option when you expect a rapid increase in the price of underlying stock.*
<br>
*__Illustration__: The biggest price movements on a % basis generally come around the time that the company releases its earnings.*
*4 times a year companies release their quarterly financial statements*
*& you should always be aware of a company's earnings release dates.*
<br>
*If you think a company is going to release very strong earnings & you don't want to spend much money buying the stock, then you may consider buying a vanilla call option.*
<br><br>
*__Example__: Suppose YHOO is at $39 & you think YHOO's stock price is going to up to $50 in the next few weeks. One way to profit from this expectation is to buy a YHOO, says 40 Calls at $2.*
*Breakeven Point Analysis/share = Strike Price + vanilla Option Premium = 40 + 2 = $42.*
<br>
*Worst case, you do not exercise, losing vanilla option premium/share = $2.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/fd-148.png")
```
</section>


# Option Application: Protective Put {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*1. To limit risk when first acquiring shares of stock (buy stock & vanilla put option)*
<br>
*2. To protect a previously-purchased stock when the short-term forecast is bearish but the long-term forecast is bullish. (buy stock then buy vanilla put option)*
<br><br>
*__Example__: Buy 100 shares ABC stock at $90 per share &/then buy 1 ABC vanilla put, strike at $90.*
<br><br>
*vanilla option premium/share = $3.15,*
<br>
*total price of the contract is $3.15 x 100 = $315*
<br>
*a stock option contract is the option to buy 100 shares;*
<br>
*that's why you must multiply the contract by 100 to get the total price.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="45%",fig.cap=""}
knitr::include_graphics("images/fd-147.png")
```
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Vanilla Option Valuation with Black Scholes Merton Model</h1>


# European vs American Style Vanilla option {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-113.png")
```


# Black Scholes Model for EUROPEAN STYLE VANILLA option {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-86.png")
```


# Historical Background  {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*The key idea behind the model is to hedge the options in*
<br>
*an investment portfolio by buying and selling the underlying asset* 
<br>
*(such as a common stock) in just the right way and as a consequence,*
<br>
*eliminate risk.*
<br><br>
*The method has later become known within finance as* 
<br>
*__continuously dynamic delta hedging__.*
</section>


# Dynamic Delta Hedging {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-122.png")
```


# Dynamic Delta Hedging {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-123.png")
```


# Valuation Approaches {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-124.png")
```


# Assumptions for the Model {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
<br>
*The option is __European Style__ & can only be exercised at Expiration.*
<br><br><br>
*Markets are efficient (i.e., market movements cannot be predicted).*
<br><br>
*There are __no transaction costs__ in buying the Vanilla Option.*
<br><br>
*The __Risk-free Rate__ (or cost of borrowing),*
*__volatility__ of the underlying are known and constant.*
<br><br>
*The __Returns__ on the underlying asset are __Normally Distributed__.*
</section>


# Black Scholes (Merton): Call {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fd-91.png")
```


# European Style Call Premium: PV of Average Payoff at Expiry {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/fd-95.png")
```


# Black Scholes (Merton): Put {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="65%",fig.cap=""}
knitr::include_graphics("images/fd-136.png")
```


# N(X) = CDF(X) Cumulative Distribution Function {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fd-92.png")
```


# Vanilla Call Pricer in Python {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/fd-118.png")
```


# Hands on: Vanilla Put Option Pricer in Python {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*Implement a Vanilla Put Option Pricer by modifying the codes below*
<br>
*and print out the answer with inputs given, on the previous page.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="85%",fig.cap=""}
knitr::include_graphics("images/fd-134.png")
```
<br><br>
*Hint:*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="55%",fig.cap=""}
knitr::include_graphics("images/fd-135.png")
```
</section> 


# Hands on: Vanilla Call Option Pricer in Excel {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*Implement the Vanilla Call Option Pricer in Excel with the same inputs*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-133.png")
```
</section> 


# Review: Black Scholes Model {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*The Black Scholes (Merton) model*
<br>
*assumes these instruments (such as stocks or futures) will have a*
<br>
*__Lognormal distribution__ of prices (__Geometric Brownian motion__)*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/fd-87.png")
```
<br>
*Using this assumption and factoring in other important variables,*
<br>
*the equation derives the Premium of an __European Style Vanilla Option.__*
</section>


# Valuation Approaches {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-124.png")
```


# Monte Carlo Simulation {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*The Monte Carlo simulations are used to model the probability of different*
<br>
*outcomes in a process that cannot easily be predicted due to the intervention of*
<br>
*random variables.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-127.png")
```
</section>


# Application of Geometric Brownian motion {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
*A continuous-time random function that can be used in mathematical finance*
<br>
*to model common stock price as the generated prices __does not fall below zero__.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/fd-114.png")
```
</section>


# Review: GBM, Lognormal distribution, Price & Return {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-128.png")
```


# Monte Carlo Simulation for Stock Price with GBM {.align_left .h1_to_h2}
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-121.png")
```


# MCS for Stock Price with GBM {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-115.png")
```


# MCS for Stock Price with GBM {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-119.png")
```


#  Hands on: MCS for Vanilla Call Pricer {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*Leverage on the previous Monte Carlo Simulation (MCS) for Common Stock Price with Geometric Brownian Motion in Python,*
<br>
*and implement a Vanilla Call Option Pricer.*
<br><br>
*Print out the answer and compare with the BSM Call Premium value*
<br><br><br>
*Hint: use __np.maximum()__  and __math.exp()__ and __.mean__*
</section> 


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Binomial Option Pricing Model</h1>


# Valuation Approaches {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-145.png")
```


# Binomial Tree Model for Option Valuation {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/fd-64.png")
```


# Binomial Tree One-Period Model {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*Over the next period, some value will change to one of 2 possible values...*
<br>
*To construct a binomial model, we need to know:*
<br>
*Beginning underlying asset value, size of the 2 possible changes,*
<br>
*Probabilities of each of these changes occurring, ...*
<br>
*& discount all the way to the Present for Vanilla Option valuation.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fd-66.png")
```
</section>


# Binomial Tree Two-Period Model {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-152.png")
```


# On Underlying Prices {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*__European Style Call Option Valuation__.*
<br>
*Initial Stock Price = $102. Strike price = $100. volatility (annualised) = 20%*
<br>
*Riskfree rate (annualised) = 2%. Time step = 0.25. Tenor = 0.5 year*
<br><br>
*The size of the possible price changes:*
<br>
*u (size of move up) = 1.07*
<br>
*d (size of move down) = 1/U = 0.935*
<br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fd-67.png")
```
</section>


# On Probabilities {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-137.png")
```


# On Expiry Date {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Calculate the Payoffs on Expiry date.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-138.png")
```
</section>


# Discounting all the way to Time 0 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*The European Style Vanilla Option Premium = $5.48/share.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-139.png")
```
</section>


# Hands on: European Style Put Premium Calculation {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-140.png")
```
 

# Refining Option Valuation {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*If we choose the parameters for a Binomial Tree & probability of up movement in the following manner,*
<br>
*the tree will closely match the mean and variance of the stock’s price over short time intervals.* 
<br><br>
*We can then use risk-neutral valuation to include dividend rate & improve relevancy.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fd-78.png")
```		
</section>


# European Style Call Valuation {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-141.png")
```


# Hands on: European Style Put Premium Calculation {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-142.png")
```
 

# Hands on: American Style Put Premium Calculation {.align_left .h1_to_h2}  
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-143.png")
```
 

# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Real Option Analysis</h1>


# Why Real Option Analysis ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Real option valuation__ or __analysis__ applies option valuation techniques for project evaluation and capital budgeting decisions.*
<br><br>
*A real option, just like a vanilla option, is the right but not the obligation to undertake a business initiative.*
<br><br>
*The business initiative may be one of these, namely, expanding, staging, contracting, deferring, or abandoning a capital investment project.*
</section>


# When Real Option exists ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*A __real option__ exists when managers can influence the size and riskiness of a project’s or an investment’s cash flows*
*by taking different actions during the project’s life or investment’s tenor.*
<br><br>
*Attentive and alert managers always look for __real options__ in projects or investments.*
<br><br>
*Smarter managers try to create __real options.__*
</section>


# Key difference {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br> 
*The key differences of __real options__ as compared to typical __financial vanilla options__*
*are that they are not traded but influenced by the option holders such as the management.* 
<br><br>
*__volatility__ may be subjective and rely on the management’s perception of uncertainty.* 
</section>


# Illustration {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br> 
*Let’s go through a project evaluation with __real option analysis.__*
<br><br><br>
*We will compare this with the traditional __NPV__ approach,*
<br>
*simplify the weighted average cost of capital __(WACC)__ to just __cost of capital,__*
<br>
*expand __binomial tree__ to __trinomial tree__ and*
<br>
*relate it back to __Black-Scholes Merton’s approach.__* 
</section>


# Case: Traditional Approach {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*The project has an initial cost of $70 million, cost of capital for the project is 10%,* 
*riskfree rate is 6% (cost of borrowing for a vanilla option) and cash flows occur for 3 years,*
*either $45 million or $30 million or $15 million for 3 years.* 
<br>
*The probabilities of them occurring are 30%, 40% and 30% respectively.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ropt-1.png")
```
</section>


# Case: With Real Option {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*If we wait 1 year, we will gain additional information regarding demand.*
<br>
*If demand is low, we will not implement the project.*
<br>
*Note: Risk-neutral probabilities will be used*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ropt-2.png")
```
</section>


# Bring-in Black Scholes model {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*We need all these input factors: Strike Price = $70, Underlying Price = PV_0,*
<br>
*Tenor = 1, Risk Free Rate = 6%. We need to find the Underlying Price and volatility ?*
<br>
*PV_0 is found to be $59.83*
<br>
*volatility is square root of the variance of the project's rate of return*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ropt-3.png")
```
</section>


# BSM model via Excel Spreadsheet {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*We can use Excel Vanilla Call Option Pricer Spreadsheet to compute this option premium.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ropt-4.png")
```
</section>


# BSM model via Python Program {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/ropt-5.png")
```


# Summary {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__This option premium__ is found to be $15.2 million*
<br><br>
*This can be interpreted as the amount to be paid in order to enjoy this option*
<br>
*that gives you the right to purchase the project for a strike price of $70 million in 1 year time.*
</section>


# Suggested Excel Functions {.align_left .h1_to_h2}  
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fd-144.png")
```
 

# Multi-Choice Question 1 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What is (are) true for Futures ?*
<br>

*a. Exchange Traded Financial Instruments*

*b. Financial Derivatives*

*c. Futures price converges to Spot (cash) price on Futures' expiry date*

*d. All of the above*
</section>


# Multi-Choice Question 2 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What is (are) true for Forwards ?*
<br>

*a. Over-The-Counter (non-Exchange-traded)*

*b. Another word for Futures*

*c. Not Financial Derivatives*

*d. Not meant for a future date or period*
</section>


# Multi-Choice Question 3 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What is (are) true for Options ?*
<br>

*a. Financial Derivatives*

*b. Has an expired date*

*c. Can be used to trade price's or interest rate's volatilities*

*d. All of the above*
</section>


# Multi-Choice Question 4 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What is (are) true for a Vanilla Call Option ?*
<br>

*a. Good for a bullish environment*

*b. It is a perpetual contract*

*c. Good for a bearish environment*

*d. All of the above*
</section>


# Multi-Choice Question 5 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What affect(s) Vanilla Option Premium ?*
<br>

*a. Spot (cash) price of the Underlying Asset*

*b. Strike (or Exercise) Price*

*c. Volatility (volatility)*

*d. All of the above*
</section>


# Multi-Choice Question 6 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*What does In-The-Money mean ?*
<br>

*a. High chance of making money*

*b. Relative Expensive compared to others (At-The-Money, Out-of-The-Money)*

*c. Spot Price > Strike Price, for Call Options*

*d. All of the above*
</section>


# Multi-Choice Question 7 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Possible Options' underlying Asset(s) is/are*
<br>

*a. Equity Index*

*b. Equity Index Futures*

*c. Equity* 

*d. All of the above*
</section>


# Multi-Choice Question 8 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*When the price volatility is up,*
<br>

*a. call option premium increases*

*b. put option premium decreases*

*c. call option premium decreases*

*d. no change in option premium*
</section>


# Multi-Choice Question 9 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*When the underlying price is up,*
<br>

*a. Put option premium increases*

*b. Call option premium decreases*

*c. Call option premium increases*

*d. no change in the option premium*
</section>


# Multi-Choice Question 10 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Getting closer to Option Expiry date,*
<br>

*a. Time Value of Options drops drastically*

*b. Intrinsic Value of Options always enhances*

*c. Time Value of Options remains unchanged*

*d. Intrinsic Value of Options always reduces*
</section>
