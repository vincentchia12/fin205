---
title: "__FIN205__
        <br>
        Data Technologies for Financial Modelling
        <br>
        <span style='font-size:75%;'>Lesson 6</span>
        <br>"
author: "<span style='color:green;font-size:60%;'>last updated on 6th Mar 2023</span>"
output:
  revealjs::revealjs_presentation:
    css: styles.css
    self_contained: true
#    reveal_plugins: ["notes", "chalkboard", "menu"]
#    transition: fade
#    reveal_options:
#      slideNumber: true
#      chalkboard:
#        theme: whiteboard
#      toggleNotesButton: false
#      menu:
#        side: right
    theme: sky
    highlight: pygments
---

# Learning Objectives {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*1.	Understand Decentralised Technology: Distributed Ledger Technology*
<br><br>
*2. Understand Centralised Technology: Anything as a Service*
<br><br>
*3.	Understand the role of FinTech in Investment Management*
<br><br>
*4.	Understand FinTech valuation via Market Sizing Approaches and etc*
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Decentralised Technology</h1>


# Decentralised Technology {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
<br><br>
*__Centralised Technology__ is when all the information comes from one computing resource, in general.* 
<br><br>
*While __Decentralised Technology__ is when all information is distributed among* 
*multiple hubs*
</section> 


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Distributed Ledger Technology</h1>
  
  
# Distributed Ledger Technology {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*__Distributed Ledger Technology (DLT)__ refers to the technological infrastructure & protocols*
*that allows simultaneous access, validation & record updating in an immutable manner*
*across a network spread across multiple entities or locations.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-15.png")
```
</section> 


# DLT vs Blockchain Technology {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*__Blockchain Technology__ is a distributed ledger (database) system that promotes decentralization, transparency & data integrity.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/dlt-14.png")
```
</section>


# DLT vs Blockchain Technology {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/dlt-19.png")
```


# Centralised vs Blockchain {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/dlt-7.png")
```


# Centralised vs Blockchain {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/dlt-8.png")
```


# Private vs Public Blockchain {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="65%",fig.cap=""}
knitr::include_graphics("images/dlt-16.png")
```


# Differences between a Crypto-coin and token {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-38.png")
```


# Bitcoin Blockchain Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-17.png")
```


# Bitcoin Transactional Universe {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
http://dailyblockchain.github.io/
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/dlt-36.png")
```
</section>


# 1. Bitcoin Blockchain Overview {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*__Bitcoin Blockchain__ is a Public Blockchain that contains the history of*
*every __secondary market Bitcoin-transactions__ as well as the __newly created__*
*__Bitcoins__ and transferred.*
<br><br>
*Anyone can download a copy of the blockchain (read only)*
<br>
*it can be inspected to trace the path of Bitcoins from 1 transaction to another.* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/dlt-23.png")
```
</section>


# Bitcoin Blockchain Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-17.png")
```


# 2. Secondary Bitcoin Transactions {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*A published block contained mainly __secondary market bitcoin__ transactions.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-25.png")
```
</section>


# Bitcoin Blockchain Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-17.png")
```


# 3. Successful Miner's earning {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*The Successful Miner earned newly created Primary Bitcoins & fees in Primary Bitcoins*  
<br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/dlt-22.png")
```
</section>


# Successful Miner's earning {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*The Successful Miner (in this case, Coinbase) earned newly generated Primary Bitcoins & fees in Primary Bitcoins = 6.250 + 1.132 = 7.382 BTC*  
<br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/dlt-24.png")
```
</section>


# Bitcoin Mining Farm {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
<br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/dlt-29.png")
```
</section>


# Bitcoin Mining Pool {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
<br>
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/dlt-27.png")
```
</section>


# Bitcoin Blockchain Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/dlt-17.png")
```


# 4. Reachable Bitcoin Mining Nodes {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
https://bitnodes.io/nodes/live-map/
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/dlt-34.png")
```
</section>


# Bitcoin Hashing Illustration {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*Bitcoin is simply a digital ledger of hashes that utilizes proof of work combined*
*with SHA-256 to obtain mathematical traceability, unbreakability & aspiring to build Trust.*
<br><br>
https://andersbrownworth.com/blockchain/blockchain
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/dlt-37.png")
```
</section>


# Historical Perspective {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*"Goat" = Secondary Bitcoin that was transferred from A to B.*
<br><br>
*"Wall Painter who was rewarded with just-borned goats = *
<br>
*"Successful Miner (or Mining Pool) that was rewarded with newly generated Primary Bitcoins & fees in Secondary Bitcoins".*
<br><br>
*"Witness" = "The Bitcoin Blockchain Network (other not-successful Miners/Nodes)".*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/dlt-1.png")
```
</section>


# Top Banks Investing in Blockchain Companies {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/BanksBlockchain.jpg")
```
 

# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">PORTFOLIO MANAGEMENT</h1>


# Portfolio Management Process {.align_left .h1_to_h2}
<br>
<section style="text-align:left;font-size:75%;">
*Portfolio perspective refers to evaluating individual investments* 
*by their contribution to the risk and return of an investor’s portfolio.* 
<br><br>
*__Modern Portfolio Theory (MPT)__ concludes that the extra risk from holding*
*only a single security is not rewarded with higher expected investment returns.* 
<br><br>
*__Diversification__ allows an investor to reduce portfolio risk*
*without necessarily reducing the portfolio’s expected return.*
</section> 


# Modern Portfolio Theory {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ft-7.png")
```
 

# Capital Market Line {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ft-8.png")
```


# Efficient Frontiers & Market Portfolios {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ft-80.png")
```


#
<section style="text-align:left;font-size:75%;">
*From __Modern Portfolio Theory's (MPT) Efficient Frontier__*
<br>
*to __Capital Market Line (CML)__ to __Security Market Line (SML)__*
<br>
*to __Captial Asset Pricing Model (CAPM)__*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="70%",fig.cap=""}
knitr::include_graphics("images/ft-10.png")
```
</section> 


# Systematic & Unsystematic Risk  {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*__Total Risk = Systematic Risk + Unsystematic Risk__*
<br><br>
*The Unsystematic Risk is affected by factors such as labor strikes,*
*part shortages, etc, that will only affect a specific firm, or a small number of firms.*
<br><br>
*Unsystematic Risk is also called Diversifiable Risk, Firm Specific Risk.*
<br>
*Unsystematic Risk can be eliminated by increasing, the relevant number of appropriate Assets in the Portfolio.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="70%",fig.cap=""}
knitr::include_graphics("images/ft-93.png")
```
</section>


# "How many Stocks are Enough" {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Reference Paper:__*
<br>
*"Equity Portfolio Diversification: How many Stocks are Enough?*
<br>
*Evidence from Five Developed Markets"*
<br><br>
*Concluding remarks drawn from the research paper:*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/ft-92.png")
```
</section> 


# Capital Market Line vs  Security Market Line {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*The __Capital Market Line (CML)__ is sometimes confused with the __Security Market Line (SML)__.* 
*The __SML__ is derived from the __CML__.* 
<br><br><br>
*While the __CML__ shows the rates of Return for a specific portfolio,*
*the __SML__ represents the Market’s Risk & Return at a given time*
*& shows the __Expected Returns__ of individual Assets.* 
<br><br><br>
*Risk measure in the __CML__ is __Standard Deviation of Returns (Total Risk)__,* 
<br>
*Risk measure in the __SML__ is __Systematic Risk__, or __Beta.__* 
</section> 


# CAPITAL ASSET PRICING MODEL (CAPM) {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*The __Capital Asset Pricing Model (CAPM)__ describes the relationship between*
*__Systematic Risk (Beta)__ & __Expected Return__ for assets, particularly common stocks.* 

*__CAPM__ is widely used throughout Finance for pricing Risky Securities* 
*& generating Expected Return for an Asset & serves as a good estimator for __Cost of Capital (Equity)__.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="55%",fig.cap=""}
knitr::include_graphics("images/financial_ratio_47.png")
```
</section> 


# Review: Beta {.align_left .h1_to_h2}
<section style="text-align:left;font-size:70%;">
<br><br>
*__Beta of Market Portfolio = 1__*
<br><br><br>
*__Beta of Risky Asset > 1__*
<br>
*indicates that the security's price is theoretically more volatile than the market.*
<br><br><br>
*__Beta of Risky Asset < 1__*
<br>
*means that the security is theoretically less volatile than the market.*
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Portfolio with cryptocurrency Index </h1>


# Cryptocurrency: A new investment opportunity {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*Paper: Lee, D. C. K., Guo, L., & Wang, Y. (2017).*
<br>
*Cryptocurrency: A new investment opportunity? The Journal of Alternative Investments, 20(3), 16-40.* 
<br><br>
*The paper investigates the diversification role of cryptocurrency and the effects of including cryptocurrency in a portfolio that consists of mainstream assets ...*
</section> 


# Cryptocurrency Market {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:75%;">
*The cryptocurrency market is attracting a large amount of funds.*
*The number of altcoins has increased a lot and altcoins are*
*playing an increasingly important role - meaning there are a lot tokens to choose from other than Bitcoin.*  
<br>
*CRIX (CRypto IndeX http://thecrix.de/) is used to represent the cryptocurrency market.* 
<br><br>
*The paper draws the comparisons between cryptocurrencies and mainstream asset classes*
*(e.g. stocks, bonds, commodities, gold, oil, PE, REITs) in 2 ways:*
<br>
*1. The differences in return and risk;* 
<br>
*2. The effect of adding cryptocurrencies to a portfolio that consists of mainstream asset classes.* 
</section> 


# Cryptocurrency as an Asset Class {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*Cryptocurrencies outperform traditional assets in terms of average daily returns.*
<br><br>
*However, this outperformance comes with a high degree of volatility or a high risk.* 
<br><br>
*CRIX (CRypto IndeX http://thecrix.de/) is used to represent the cryptocurrency market.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/ft-88.png")
```
</section>


# Efficient Frontier Shifted Upwards {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*__Correlations__ between cryptocurrencies & mainstream asset classes are very low*
*(e.g., correlation of CRIX with the S&P 500 is 0.036).*
*So cryptocurrency could be a promising investment to hedge portfolio risks.* 
<br><br>
*Including CRIX in a portfolio of mainstream assets shifts the efficient frontier upwards,*
*meaning the entire portfolio has better performance under the same level of risk.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/ft-90.png")
```
</section>


# Sharpe Ratio {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*The __Sharpe Ratio__ measures excess Return per unit of Risk.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/ft-32.png")
```
<br><br>
*The term Rp – Rf is the __Excess Return on Portfolio__.*
<br><br>
*It measures the extra reward that investors receive for exposing themselves to Risk.*
<br><br>
*Portfolio with large positive __Sharpe Ratios__ are preferred to Portfolios with smaller __Sharpe Ratios__.*
</section> 


# Year 2017 Sharpe Ratio Comparison {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*Reference Paper: Lee, D. C. K., Guo, L., & Wang, Y. (2017).*
<br>
*Cryptocurrency: A new investment opportunity? The Journal of Alternative Investments, 20(3), 16-40.* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="55%",fig.cap=""}
knitr::include_graphics("images/ft-91.png")
```
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Centralised Technology</h1>


# Centralised Technology {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
<br>
*Centralised Technology is when all the information comes from one computing resource, in general.* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fto-31.png")
```
</section>


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Anything as a Service</h1>


# Anything as a Service, XaaS {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*__Anything as a Service, XaaS__ is a term that describes a broad category of services*
*related to cloud computing and remote access.*
<br>
*As such, the list of examples is endless.*
<br><br>
*The common ones are:*
<br>
*1. __SaaS__: Software as a Service*
<br>
*2. __PaaS__: Platform as a Service*
<br>
*3. __IaaS__: Infrastructure as a Service*
<br>
*4. __AaaS__: Analytics as a Service ... *
<br><br><br>
*With so many kinds of IT resources now delivered this way, __XaaS__ is a somewhat ironic term*
*for the proliferation of subscription-based cloud services*
</section> 


# Benefits of XaaS {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*There are several benefits of __XaaS__:*
<br><br><br>
*1. Improving the expense model*
<br><br>
*2. Speeding new apps and business processes*
<br><br>
*3. Shifting IT resources to higher-value projects*
</section> 


# 1. Improving the expense model {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*With __XaaS__, businesses can cut costs by purchasing services from providers on a subscription basis.*
<br><br>
*Before __XaaS__, businesses had to purchase individual products—software, hardware, servers, security, infrastructure and etc,*
<br>
*install them on site, and then integrate everything together to create a connected network of devices.* 
<br><br>
*Now, with __XaaS__, businesses simply purchase what they need essentially, and pay as they go.* 
<br><br>
*Previous capital expenses (__CapEx__ in balance sheet) now become operating expenses (__OpEx__ in Income Statement).*
</section> 


# 2. Speeding new apps and business processes {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*This model allows businesses to quickly adapt to changing market conditions with new apps or solutions.*
<br><br>
*Using multi-tenant approaches, cloud services can provide much-needed flexibility.*
<br><br>
*Resource pooling and rapid elasticity support mean that a company can simply add or remove services as needed.*
<br><br>
*A company can quickly access new technologies, scaling infrastructure automatically when users need innovative resources.*
</section> 


# 3. Shifting IT resources to higher-value projects {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Shifting IT resources to higher-value projects.*
<br><br>
*Increasingly, IT organizations are turning to a XaaS delivery model to streamline operations and free up resources for innovation.*
<br><br>
*They are also using the benefits of XaaS to transform digitally and become more agile.*
<br><br>
*From Deloitte's survey, 71% of companies reported that XaaS now constitutes more than half of their company’s enterprise IT.*
<br><br>
*__XaaS__ provides more users with access to cutting-edge technology, democratising innovation.*
</section> 


# Disadvantages of XaaS {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*__XaaS__ has some potential disadvantages:*
<br><br><br>
*1. Possible downtime*
<br><br>
*2. Performance issues*
<br><br>
*3. Complexity impacts.*
</section>


# 1. Possible downtime {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*The internet sometimes breaks-down, and when it does, your __XaaS__ provider might have problems as well.*
<br><br>
*With __XaaS__, there can be issues of internet reliability, resilience, provisioning and managing the infrastructure resources.*
<br><br><br>
*If __XaaS__ servers go down, users won’t be able to use them.*
<br><br>
*__XaaS__ providers can guarantee services through Service-Level Agreements, __SLA__.*
</section> 


# 2. Performance issues {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*As __XaaS__ becomes more popular, bandwidth, latency, data storage, and retrieval times can suffer.*
<br><br>
*If too many customers use the same resources, the system can slow down.*
<br><br>
*Apps running in virtualised environments can also face impacts.*
<br><br>
*In these complex environments, there can be integration issues,*
*including the ongoing management and security of multiple cloud services.*
</section> 


# 3. Complexity impacts {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*Pushing technology to __XaaS__ can relieve IT staff of day-to-day operational headaches;*
*however, if something does go wrong, it might be harder to troubleshoot.*
<br><br>
*The internal IT staff still needs to stay current on the new technology.*
<br>
*Costs for maintaining high-performing,*
*robust networks can increase — although the overall cost savings of __XaaS__ models are usually much greater.*
<br><br>
*Nonetheless, some companies want to retain visibility into their __XaaS__ service provider’s environment and infrastructure.*
<br><br>
*In addition, a __XaaS__ provider that gets acquired, discontinues a service, or alters its roadmap can have a profound impact on __XaaS__ users.* 
</section> 


# Example: Software as a service {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Software as a service, SaaS__ is a cloud-based software delivery model*
*in which the cloud provider develops and maintains cloud application software,*
*provides automatic software updates,*
*and makes software available to its customers via the internet on a pay-as-you-go basis.*
<br><br>
*The public cloud provider manages all the hardware and traditional software,*
*including middleware, application software, and security.*
<br><br>
*So __SaaS__ customers can dramatically lower costs; deploy, scale,*
*and upgrade business solutions more quickly than maintaining on-premises systems and software;*
*and predict total cost of ownership with greater accuracy.*
<br><br>
*Example: Microsoft Power BI service*
</section>


# Example: Platform as a service {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br><br>
*__Platform as a service, PaaS__ is a category of cloud computing services that* 
*allows customers to provision, instantiate, run, and manage a modular bundle*
<br>
*comprising a computing platform and one or more applications,*
<br>
*without the complexity of building and maintaining the infrastructure*
<br>
*typically associated with developing and launching the application(s);*
<br>
*and to allow developers to create, develop, and package such software bundles*
<br><br><br>
*Example: Google App Engine*
</section>


# Example: Infrastructure as a Service {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Infrastructure as a service (IaaS)__ is the on-demand availability of*
*almost infinitely scalable computing resources as services over the internet.*
<br><br>
*It eliminates the need for enterprises to procure, configure,*
*or manage infrastructure themselves, and they only pay for what they use.*
<br><br>
*__IaaS__ provides enterprises with storage, server, and networking options*
*that don’t require them to purchase and maintain vast private server rooms*
*that take up a lot of energy and space.*
<br><br>
*Thus one of the key reasons businesses choose __IaaS__ is to reduce*
*their capital expenditures and transform them into operational expenses.*
</section>


# Example: Analytics as a Service {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Analytics-as-a-Service (AaaS)__ provides subscription-based data analytics software and procedures through the cloud.*
<br><br>
*__AaaS__ typically offers a fully customizable Business Intelligence solution with end-to-end*
*capabilities, organizing, analyzing, and presenting data in a way that*
*lets even non-IT professionals gain insight and take action.*
<br><br>
*The ubiquitous rise of Big Data and the astronomic expenses of parsing these massive datasets*
*is leading __CIOs__ to opt for __AaaS’__ cost-effective web product over traditional licensed and on-premise solutions.*
<br><br><br>
*Example: Mixpanel*
</section>


# Summary {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/xaas-1.png")
```


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Role of Fintech in Investment Management</h1>


# What is a Fintech ? {.align_left .h1_to_h2}  
<section style="text-align:left;font-size:75%;">
*A __Fintech__ firm refers to a company that operates in the financial services sector* 
*and leverages the power of technology to simplify,* 
*automate & improve the delivery of financial services to end customers.*
<br><br>
*Global investment activity in __Fintechs__ exceeded $30 billion across 450 deals* 
*that took place in the first half of 2018 alone.*
<br><br>
*In comparison to brick-and-mortar Banks, Asset Management companies,*
*Insurance companies,*
<br><br>
*__Fintech__ companies have some of the following characteristics:*
<br>
*Unpredictable or negative cash flows,*
<br>
*Negligible physical assets,*
<br>
*Rapidly changing pivoting business models.*
</section> 


# Singapore in Number {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-1.png")
```


# Ultra Connected Population {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fto-2.png")
```


# Singapore FinTech Map 1 {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-3.png")
```


# Singapore FinTech Map 2 {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fto-4.png")
```


# Singapore FinTech Breakdown {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/fto-5.png")
```


# Role of Fintech in Investment Management {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*__Some primary areas:__*
<br>
*Increasing functionality to handle large sets of data that may come from many sources and exist in a variety of forms.*
<br><br>
*Tools and techniques such as artificial intelligence for analysing very large data sets.*
<br><br>
*Automation of financial functions such as executing trades and providing investment advice.*
<br><br>
*Emerging technologies for financial recordkeeping that may reduce the need for intermediaries.*
</section> 


# Role of Fintech in Investment Management {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
<br>
*__Technologies:__*
<br><br>
*Artificial intelligence, such as __AI as a Service__*
<br>
*Distributed ledger technology, such as Private Blockchain*
<br>
*Trusted hardware*
<br>
*Internet of Things, __IOT__*
<br>
*Smart contract, such as Ethereum's Smart Contract*
<br><br><br>
*Requires Account Managers to understand the Risks of the Technologies.*
</section> 


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Fintech Valuation</h1>


# Why Valuation ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:80%;">
<br><br>
*Reasons are many, some of them are:*
<br><br>
*A basis to seek funding to grow the business further.*
<br><br>
*An approach to assessing investment opportunity.*
</section> 


# How does a Startup's funding looks like ? {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-10.png")
```


# Who are Angel (investors) ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*An __angel investor__ (also known as a private investor, seed investor or angel funder)*
*is a high-net-worth individual who provides financial backing for small startups or entrepreneurs,*
*typically in exchange for ownership equity in the company.*
<br>
*They are individual style driven.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="75%",fig.cap=""}
knitr::include_graphics("images/fto-11.png")
```
</section>


#
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-12.png")
```


# Who are VC ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*__Venture capital (VC)__ is a form of private equity and a type of financing that*
*investors provide to startup companies and small businesses*
*that are believed to have long-term growth potential.*
<br>
*They are process driven.*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="80%",fig.cap=""}
knitr::include_graphics("images/fto-13.png")
```
</section>


# Why stage A, B, C ... ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:80%;">
<br><br>
*Reasons are many, some of them are:*
<br><br>
*Step by step in borrowing and expanding*
<br><br>
*Divide the borrowing process into bite-size.*
<br><br>
*Minimise equity diluation/participants.*
<br><br>
*Not enough money, need more to pull through.*
</section> 


# Fintech valuation READING PAPER {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
https://www.toptal.com/finance/market-sizing/total-addressable-market-example
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-30.png")
```
</section> 


# 
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/ft-49.png")
```
 

# TAM SAM SOM {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/TAM-0.png")
```


# Why Market Sizing Valuation ? {.align_left .h1_to_h2}
<section style="text-align:left;font-size:80%;">
<br>
*One of the most important evaluation criteria for a business idea:*
<br>
*__The size of its market.__*
<br><br>
*If it is too small, the investment is not worthwhile and the idea cannot be realised.*
<br><br>
*For the very reason of worthiness,* 
*an estimate of the market size is a mandatory part of every business case or*
*business plan that must be presented in a company or by a startup in order to*
*receive funding for the development of the idea.* 
</section>


# 1. External Research {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*An easy & quick way of finding a TAM is to reference professional data* 
*that has already been collected.* 
<br>
*A brief search yielded a press release from Gartner stating that the* 
*cybersecurity market was worth $81.6 billion in 2016.*
<br>
*An example of research report by Gartner Research*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="65%",fig.cap=""}
knitr::include_graphics("images/TAM-2.png")
```
</section>


# 2. Top Down Approach {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*The top down approach takes the ethos that you start at the very top of a macro data set*
*and chip away at the data to find a market subset.*
*You start with a population and then logically apply demographic, geographic*
*and economic assumptions to eliminate irrelevant segments.*
<br><br>
*The advantage of this method is that accurate & open statistics can be found*
*for macroeconomic data.An example of a fact-page on World Bank's website*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="60%",fig.cap=""}
knitr::include_graphics("images/TAM-3.png")
```
</section>


# Top Down Approach Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-14.png")
```
 
 
# Example: {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-15.png")
```


# Example: {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-16.png")
```


# Example: {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-17.png")
```


# Example: {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-18.png")
```

# 3. Bottom Up Approach {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*This method involves working on a granular level to find waypoints that are extrapolated up*
*to the wider population.*
*It is favored for being more accurate, because its basis is anchored around a proven data point,*
*which can be magnified to uncover the whole TAM population.*
*One can use primary collection methods (such as a survey in a local market),*
*(or secondary research (news reports, company filings).* 
<br>
*Example on statistics and facts on Tesco PLC*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="50%",fig.cap=""}
knitr::include_graphics("images/TAM-4.png")
```
</section>


# An Illustration {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-19.png")
```
 

# 4. Value Theory Approach {.align_left .h1_to_h2}
<section style="text-align:left;font-size:65%;">
*Top down and bottom up methods usually look at existing paradigms & assume*
*that a new offering will fit into them*
*This may not be applicable for new frontier companies or technologies*
<br><br>
*With value theory, one must assess how much a customer would be willing to pay*
*for an improvement/evolution of a product*
*Such an exercise would have been useful when music streaming subscription services,*
*like Spotify, started to roll out.* 
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="55%",fig.cap=""}
knitr::include_graphics("images/TAM-5.png")
```
</section>


# Why Market Sizing Valuation is attracting attention ? {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/fto-20.png")
```


# Suggested Valuation approach (if data is available) {.align_left .h1_to_h2}
<section style="text-align:left;font-size:60%;">
*1. Calculate SOM, Serviceable Obtainable Market. Aim to achieve 10% in first year*
 
*2. Multiply 10% x SOM x Average Industrial Net Profit Margin = Net Profit*

*3. Estimate the Dividend (per share) with relevant <Average Industrial Dividend percentage & estimated number of shares>*

*4. Use a constant growth Dividend Discount Model to value Market Price per share,*
*with <cost of equity or discount rate calculated from CAPM (competitor's stock) & an estimated growth rate, g>*
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="40%",fig.cap=""}
knitr::include_graphics("images/TAM-12.png")
```
</section>


# Homework: Fintech valuation READING PAPER {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
https://www.toptal.com/finance/valuation/how-to-value-a-fintech-startup
</section> 


# >> {.section_title .hide_h1}
<br><br><br><br>
<hr>
<h1 style="display:block;">Python-Excel interoperability</h1>


# Merge 2 Excel Spreadsheets {.align_left .h1_to_h2}
<br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/TAM-13.png")
```


# Merge 2 Excel Spreadsheets {.align_left .h1_to_h2}
<br><br>
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/TAM-16.png")
```


# Extract data from an Excel Spreadsheet {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="100%",fig.cap=""}
knitr::include_graphics("images/TAM-14.png")
```


# Extract data from an Excel Spreadsheet {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="25%",fig.cap=""}
knitr::include_graphics("images/TAM-17.png")
```


# Compute data in an Excel Spreadsheet {.align_left .h1_to_h2}
```{r, echo=FALSE,fig.align="center",out.width="90%",fig.cap=""}
knitr::include_graphics("images/TAM-15.png")
```


# Multi-Choice Question 1 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Which statement is false ?*
<br>

*a. All Blockchains are distributed ledgers*  

*b. All distributed ledgers are Blockchains* 

*c. A DLT needs no data structure or centralised confirmation* 
 
*d. Blockchain can be used interchangeably with Distributed Ledger Technology*  
</section>


# Multi-Choice Question 2 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Which statement about Blockchain(s) is(are) true ?*
<br>

*a. Some are private* 

*b. Some are public* 

*c. Can be categorised as private or public* 

*d. All of the above*  
</section>


# Multi-Choice Question 3 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Modern Portfolio Theory suggested that ?*
<br>

*a. Efficient portfolio cannot be constructed* 

*b. Efficient frontier cannot be constructed*

*c. Some efficient portfolio lie on the efficient frontier*

*d. All efficient portfolio lie on the efficient frontier*
</section>
 

# Multi-Choice Question 4 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Valuation via Market Sizing Approach includes*
<br>

*a. External Research.* 

*b. Top Down Approach.*   

*c. Bottom Up Approach.*

*d. All of the above.* 
</section>


# Multi-Choice Question 5 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Capital Market Line and Security Market Line deal with*
<br>

*a. Total risk and Systematic risk respectively*

*b. Systematic risk and Total risk respectively*

*c. Total risk and Unsystematic risk respectively*

*d. Unsystematic risk and Systematic risk respectively*
</section>


# Multi-Choice Question 6 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Capital Asset Pricing Model is used*
<br>

*a. to price risky securities*

*b. to generate expected returns of an asset*

*c. to calculate cost of equity*

*d. all of the above*
</section>


# Multi-Choice Question 7 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Beta of Market Portfolio*
<br>

*a. is greater than 1*

*b. is lower than 1*

*c. is equal to 1*

*d. can be any number*
</section>


# Multi-Choice Question 8 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Sharpe ratio allows onne to measure calculated risk. It is defined as*
<br>

*a. Portfolio return divided by standard deviation of portfolio returns.*

*b. Risk-free return divided by standard deviation of portfolio returns.*

*c. [Portfolio return - Riskfree return] divided by standard deviation of portfolio returns.*

*d. Portfolio return divided by Risk-free return.*
</section>


# Multi-Choice Question 9 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*XaaS: Anything as a Service includes*
<br>

*a. SaaS: Software as a Service.*

*b. PaaS: Platform as a Service.*

*c. IaaS: Infrastructure as a Service.*

*d. All of the above.*
</section>


# Multi-Choice Question 10 {.align_left .h1_to_h2}
<section style="text-align:left;font-size:75%;">
*Which statement is false on Market Sizing Valuation Approach ?*
<br>

*a. TAM stands for Total Addressable Market.*  

*b. SAM stands for Serviceable Addressable Market.* 

*c. SOM stands for Serviceable Obtainable Market.* 

*d. COM stands for Current Obtainable Market.*
</section>
